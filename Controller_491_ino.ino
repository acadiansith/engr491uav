/***************************************************************************
Controller Code                                                            *
By: Stephen Tyler                                                          *
Purpose:  Send data given from the equipment on a remote control to a UAV's*
          onboard computer.  The data will be sent with a starting and     *
          ending message.  After the starting message, bytes 5 and 4 will  *
          be sent and contain the pitch information.  Bytes 3 and 2 will   *
          contain the roll information. Byte 1 will contain the yaw        *
          information. Byte 0 will contain information on all push buttons,*
          including the altitude and the release mechanism.                *
Jan 15, 2014 -> Created                                                    *
Jan 16, 2014 -> Added line for encoder to update in loop                   *
Jan 17, 2014 -> Adjusted ALTUP and ALTD readings, put main code in a       *
                seperate while loop, put in encoder push button, set to    *
                write information to Serial, put in LED code               *
Feb 2, 2014  -> Adjusted code to replace rotary encoder with a three way   *
                switch                                                     *
Feb 10, 2014 -> Adjusted code for the edition of one other momentary       *
                button.                                                    *
***************************************************************************/

const int VERT = 0; // analog input
const int HORIZ = 1; // analog input
const int SWI1 = 2; //digital input
const int SWI2 = 3; //digital input
const int SELThumb = 4; // digital input
const int ALTUP = 5; //digital input
const int ALTD = 6; //digital input
const int REL = 7;  //digital input
const int LEDLatch = 8; //digital output
const int HALF = 500;

void setup()
{
  // set up serial port for output
  Serial.begin(9600);
  
  // make appropriate pins inputs
  pinMode(SWI1, INPUT);
  pinMode(SWI2, INPUT);
  pinMode(SELThumb,INPUT);
  pinMode(ALTUP, INPUT);
  pinMode(ALTD, INPUT);
  pinMode(REL, INPUT);
  pinMode(LEDLatch, OUTPUT);
  
  //Set the internal pullups to be on. This makes all buttons pressed give a LOW Signal.
  digitalWrite(SWI1, HIGH);
  digitalWrite(SWI2, HIGH);
  digitalWrite(SELThumb,HIGH);
  digitalWrite(ALTUP, HIGH);
  digitalWrite(ALTD, HIGH);
  digitalWrite(REL, HIGH);
  
}

void loop() 
{
  int vertical, horizontal, selT, l_yaw, r_yaw, up, down, relMech;
  byte yaw = 0; //Byte 1
  byte switches = 0; //Byte 0
  
  char val = Serial.read();
  int acc = 'b';
  int n;
  //Check for Wireless communication with UAV
  while(val != acc){
    val = Serial.read();           //Checks incomeing signal from the UAV
    delay(1);                   //Delays for half a second
    n++;
    if(n == 500){
      digitalWrite(LEDLatch, !digitalRead(LEDLatch));
      n = 0;
    }
  }
  
  
  digitalWrite(LEDLatch, HIGH);  //Turns LED on Power button on
  

   vertical = analogRead(VERT); // will be 0-1023
   horizontal = analogRead(HORIZ); // will be 0-1023
   selT = digitalRead(SELThumb); // will be HIGH (1) if not pressed, and LOW (0) if pressed
   l_yaw = digitalRead(SWI1);// will be HIGH (1) if not pressed, and LOW (0) if pressed
   r_yaw = digitalRead(SWI2);// will be HIGH (1) if not pressed, and LOW (0) if pressed
   up = digitalRead(ALTUP); //Will be HIGH (1) if not pressed, and LOW otherwise
   down = digitalRead(ALTD); //Will be HIGH (1) if not pressed, and LOW otherwise
   relMech = digitalRead(REL); //Will be HIGH if not pressed, and LOW otherwise
   // print out the values 
   
   Serial.write("S");  //Starting Message
   //Serial.print("Pitch: ");  //Commented out as was part of previous code that showed seperate values in Serial Monitor
   Serial.write((byte*)&vertical, 2);  //Bytes 5 & 4, contain Pitch information
   //Serial.print(vertical);
   //Serial.print(" Roll: ");
   Serial.write((byte*)&horizontal, 2); //Bytes 3 & 2, contain Roll information
  // Serial.print(horizontal);
   //Serial.print(" Yaw: ");
   if(l_yaw == LOW)    //Information in Byte 1
     yaw += B0001;    //Bit 4 in message identifies a to rotate to the left
   if(r_yaw == LOW)
     yaw += B0010;    //Bit 5 in message identifies a to rotate to the right
   Serial.write(yaw);
   //Serial.print(" Alt: ");
   if(up == LOW)      //Information in Byte 0
     switches += B0001; //Bit 0 in message identifies a command to increase altitude
  
   if(down == LOW)
     switches += B0010; //Bit 1 in message identifies a command to decrease altitude

   //Serial.print(" Thumb: ");
   if(selT == LOW)
     switches += B0100; //Bit 2 in message identifies that the push button in the joy stick has been pressed
   
   if(relMech == LOW)
     switches += B1000; //Bit 3 in message identifies a command to open the claw
     
  Serial.write(switches);
  
}  
