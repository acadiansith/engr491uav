/**** i2c.cpp ********************************************
 * Author: Daniel LeJeune
 * Creation: 2/3/14
 **** Changelog ******************************************
 * 
 * 3/1/14
 * ----------
 * +Combined sensor routines into one routine for speed
 *
 * 2/14/14
 * ----------
 * +Moved header information to i2c.h
 * 
 * 2/11/14
 * ----------
 * +Added HMC5883L configuration
 * +Added HMC5883L thread routine
 * +Added most-recent queue for HMC5883L
 *
 * 2/10/14
 * ----------
 * +Added ITG3200 configuration
 * +Added ITG3200 thread routine
 * +Added most-recent queue for ITG3200
 * 
 * 2/8/14
 * ----------
 * +Added most-recent queue for ADXL345
 *
 * 2/7/14
 * ----------
 * +Added configuration for ADXL345
 * 
 * 2/3/14
 * ----------
 * +File creation
 * +Added wiringPi library
 * +Created macros for wiringPi function calls
 * +Created ADXL345 thread routine
 * +Created ACCEL_AXIS_DATA union
 *********************************************************/

/**** Includes *******************************************/
#include "i2c.h"

/**** Global variables ***********************************/
pthread_t i2c_thread;
bool i2c_running, sensors_ready;
struct timespec i2c_wait;

/**** Functions ******************************************/
void begin_i2c(void) {
  i2c_wait.tv_sec = 0;
  i2c_wait.tv_nsec = I2C_WAIT_TIME;

  pthread_mutex_init(&accel_queue_mutex, NULL);
  pthread_mutex_init(&gyro_queue_mutex, NULL);
  pthread_mutex_init(&compass_queue_mutex, NULL);
  pthread_mutex_init(&arduino_mutex, NULL);

  i2c_running = true;
  sensors_ready = false;

  for (int i = 0; i < 4; i++) {
    motor_values[i] = 40;
  }
  release = false;

  pthread_create(&i2c_thread, NULL, i2c_routine, NULL);

  while (!sensors_ready) {
    nanosleep(&i2c_wait, NULL);
  }
}

void close_i2c(void) {  
  i2c_running = false;

  pthread_join(i2c_thread, NULL);

  pthread_mutex_destroy(&accel_queue_mutex);
  pthread_mutex_destroy(&gyro_queue_mutex);
  pthread_mutex_destroy(&compass_queue_mutex);
}

void* i2c_routine(void* ptr) {
  
  int ADXL345 = accel_initializeADXL345();
  int ITG3200 = gyro_initializeITG3200();
  int HMC5883L = compass_initializeHMC5883L();
  int ARDUINO = wiringPiI2CSetup(ARDUINO_DEVICE);

  // Fill up the queues before use
  pthread_mutex_lock(&accel_queue_mutex);
  for (int i = 0; i < ACCEL_QUEUE_LEN; i++) {
    while (!accel_get_data(ADXL345, accel_queue[i])) {
      nanosleep(&i2c_wait, NULL);
    }
  }
  accel_queue_head = 0;
  pthread_mutex_unlock(&accel_queue_mutex);

  pthread_mutex_lock(&gyro_queue_mutex);
  for (int i = 0; i < GYRO_QUEUE_LEN; i++) {
    while (!gyro_get_data(ITG3200, gyro_queue[i])) {
      nanosleep(&i2c_wait, NULL);
    }
  }
  gyro_queue_head = 0;
  pthread_mutex_unlock(&gyro_queue_mutex);

  pthread_mutex_lock(&compass_queue_mutex);
  for (int i = 0; i < COMPASS_QUEUE_LEN; i++) {
    while (!compass_get_data(HMC5883L, compass_queue[i])) {
      nanosleep(&i2c_wait, NULL);
    }
  }
  compass_queue_head = 0;
  pthread_mutex_unlock(&compass_queue_mutex);

  sensors_ready = true;

  while (i2c_running) {
    pthread_mutex_lock(&accel_queue_mutex);
    int accel_queue_head_new = (accel_queue_head + 1) % ACCEL_QUEUE_LEN;
    if (accel_get_data(ADXL345, accel_queue[accel_queue_head_new])) {
      accel_queue_head = accel_queue_head_new;
    }
    pthread_mutex_unlock(&accel_queue_mutex);

    pthread_mutex_lock(&gyro_queue_mutex);
    int gyro_queue_head_new = (gyro_queue_head + 1) % GYRO_QUEUE_LEN;
    if (gyro_get_data(ITG3200, gyro_queue[gyro_queue_head])) {
      gyro_queue_head = gyro_queue_head_new;
    }
    pthread_mutex_unlock(&gyro_queue_mutex);

    pthread_mutex_lock(&compass_queue_mutex);
    int compass_queue_head_new = (compass_queue_head + 1) % COMPASS_QUEUE_LEN;
    if (compass_get_data(HMC5883L, compass_queue[compass_queue_head])) {
      compass_queue_head = compass_queue_head_new;
    }
    pthread_mutex_unlock(&compass_queue_mutex);

    pthread_mutex_lock(&arduino_mutex);
    byteWrite(ARDUINO, 0x03, motor_values[0]);
    byteWrite(ARDUINO, 0x04, motor_values[1]);
    byteWrite(ARDUINO, 0x05, motor_values[2]);
    byteWrite(ARDUINO, 0x06, motor_values[3]);
    byteWrite(ARDUINO, 0x07, release?1:0);
    byteWrite(ARDUINO, 0x08, motor_start?1:0);
    pthread_mutex_unlock(&arduino_mutex);

    nanosleep(&i2c_wait, NULL);
  }

}

/****** Accelerometer functions **************************/
int accel_initializeADXL345(void) {
  
  int ADXL345 = wiringPiI2CSetup(ADXL345_DEVICE);
  
  byteWrite(ADXL345, 0x1E, 0xFD); // Set X-axis offset to -46.88mg
  byteWrite(ADXL345, 0x1F, 0x00); // Set Y-axis offset to +00.00mg
  byteWrite(ADXL345, 0x20, 0x06); // Set Z-axis offset to +93.75mg
  byteWrite(ADXL345, 0x2C, 0x0A); // Set 100Hz data rate
  byteWrite(ADXL345, 0x2D, 0x08); // Set mode to measure
  byteWrite(ADXL345, 0x31, 0x01); // Set range to 4g

  return ADXL345;
}

bool accel_get_data(int device, three_axis_data &data) {

  int INT_SOURCE = byteRead(device, 0x30);

  if (!(INT_SOURCE & 0x80)) return false;

  data.x.bytes.low = byteRead(device, 0x32);
  data.x.bytes.high = byteRead(device, 0x33);
  data.y.bytes.low = byteRead(device, 0x34);
  data.y.bytes.high = byteRead(device, 0x35);
  data.z.bytes.low = byteRead(device, 0x36);
  data.z.bytes.high = byteRead(device, 0x37);

  return true;

}

/****** Gyroscope functions ******************************/
int gyro_initializeITG3200(void) {
  
  //pthread_mutex_lock(&i2c_mutex);

  int ITG3200 = wiringPiI2CSetup(ITG3200_DEVICE);
  
  byteWrite(ITG3200, 0x15, 0x09); // Set sample rate to 100Hz
  byteWrite(ITG3200, 0x16, 0x1A); // Set range to +/-2000deg/sec,
                                  // filter bandwidth to 98Hz,
				  // internal sample rate to 1kHz
  byteWrite(ITG3200, 0x17, 0x05); // Enable data and device ready interrupts
  byteWrite(ITG3200, 0x3e, 0x01); // Set clock source to X Gyro

  // Give the device time to wake up
  while (!(byteRead(ITG3200, 0x1a) & 0x05)) {
    //pthread_mutex_unlock(&i2c_mutex);
    nanosleep(&i2c_wait, NULL);
    //pthread_mutex_lock(&i2c_mutex);
  }

  //pthread_mutex_unlock(&i2c_mutex);

  return ITG3200;
}

bool gyro_get_data(int device, three_axis_data &data) {

  int INT_SOURCE = byteRead(device, 0x1a);

  if (!(INT_SOURCE & 0x01)) return false;

  data.x.bytes.high = byteRead(device, 0x1d);
  data.x.bytes.low = byteRead(device, 0x1e);
  data.y.bytes.high = byteRead(device, 0x1f);
  data.y.bytes.low = byteRead(device, 0x20);
  data.z.bytes.high = byteRead(device, 0x21);
  data.z.bytes.low = byteRead(device, 0x22);

  return true;

}

/****** Compass functions ******************************/
int compass_initializeHMC5883L(void) {
  
  int HMC5883L = wiringPiI2CSetup(HMC5883L_DEVICE);
  
  byteWrite(HMC5883L, 0x00, 0x18); // Set 1 sample average/output
                                   // Data output rate to 75Hz
  byteWrite(HMC5883L, 0x01, 0x20); // Set range to +/-1.3Ga
  byteWrite(HMC5883L, 0x02, 0x00); // Set mode to continuous-measurement

  // Give the device time to wake up
  while (!(byteRead(HMC5883L, 0x09) & 0x01)) {
    nanosleep(&i2c_wait, NULL);
  }

  return HMC5883L;
}

bool compass_get_data(int device, three_axis_data &data) {

  int INT_SOURCE = byteRead(device, 0x09);

  if (!(INT_SOURCE & 0x01)) return false;

  data.x.bytes.high = byteRead(device, 0x03);
  data.x.bytes.low = byteRead(device, 0x04);
  data.y.bytes.high = byteRead(device, 0x07);
  data.y.bytes.low = byteRead(device, 0x08);
  data.z.bytes.high = byteRead(device, 0x05);
  data.z.bytes.low = byteRead(device, 0x06);

  return true;

}
