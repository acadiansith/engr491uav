/**** i2c.h **********************************************
 * Author: Daniel LeJeune
 * Creation: 2/14/14
 **** Changelog ******************************************
 * 
 * 2/14/14
 * ----------
 * +File creation
 *********************************************************/

#ifndef ENGR491_I2C
#define ENGR491_I2C

/**** Includes *******************************************/
#include <iostream>
#include <wiringPiI2C.h>
#include <pthread.h>
#include <cstdio>
#include <time.h>

/**** Macro definitions **********************************/
#define byteRead(A, B) wiringPiI2CReadReg8(A, B)
#define byteWrite(A, B, C) wiringPiI2CWriteReg8(A, B, C)

/**** Constant definitions *******************************/
#define ACCEL_QUEUE_LEN 100
#define GYRO_QUEUE_LEN 10
#define COMPASS_QUEUE_LEN 10

#define ARDUINO_DEVICE 0x49
#define ADXL345_DEVICE 0x53
#define ITG3200_DEVICE 0x68
#define HMC5883L_DEVICE 0x1e

#define I2C_WAIT_TIME 3300000

using namespace std;

/**** Type definitions ***********************************/
union WORD_BUILDER {
  struct {
    char low;
    char high;
  } bytes;
  short int word;
};

typedef struct {
  union WORD_BUILDER x;
  union WORD_BUILDER y;
  union WORD_BUILDER z;
} three_axis_data;

/**** Function prototypes ********************************/
void begin_i2c(void);
void close_i2c(void);
void* i2c_routine(void* ptr);

int accel_initializeADXL345(void);
bool accel_get_data(int device, three_axis_data &data);

int gyro_initializeITG3200(void); 
bool gyro_get_data(int device, three_axis_data &data); 

int compass_initializeHMC5883L(void); 
bool compass_get_data(int device, three_axis_data &data); 

/**** Global variables ***********************************/

// accelerometer data queue
extern three_axis_data accel_queue[ACCEL_QUEUE_LEN];
extern int accel_queue_head;
extern pthread_mutex_t accel_queue_mutex;

// gyroscope data queue
extern three_axis_data gyro_queue[GYRO_QUEUE_LEN];
extern int gyro_queue_head;
extern pthread_mutex_t gyro_queue_mutex;

// compass data queue
extern three_axis_data compass_queue[COMPASS_QUEUE_LEN];
extern int compass_queue_head;
extern pthread_mutex_t compass_queue_mutex;

extern int motor_values[4];
extern bool release;
extern bool motor_start;
extern pthread_mutex_t arduino_mutex;


#endif
