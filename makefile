#**** makefile *******************************************
#* Author: Daniel LeJeune
#* Creation: 2/14/14
#**** Changelog ******************************************
#* 
#* 2/14/14
#* ----------
#* +File creation
#*********************************************************/

CC=g++
SOURCES=$(wildcard *.cpp)
OBJECTS=$(SOURCES:.cpp=.o)
LIBS=-lwiringPi -pthread -lm -lrt
EXEC=uav

$(EXEC): $(OBJECTS)
	$(CC) -o $(EXEC) $(OBJECTS) $(LIBS)

%.o: %.cpp
	$(CC) -o $@ -c $<

