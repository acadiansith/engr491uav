/**** quat.cpp *******************************************
 * Author: Daniel LeJeune
 * Creation: 2/24/14
 **** Changelog ******************************************
 * 
 * 2/24/14
 * ----------
 * +File creation
 *********************************************************/

#include "quat.h"

Quat::Quat(void) {
  a = 0;
  b = 0;
  c = 0;
  d = 0;
}

Quat::Quat(double a1, double b1, double c1, double d1) {
  a = a1;
  b = b1;
  c = c1;
  d = d1;
}

double Quat::dot(const Quat &q1) {
  return a*q1.a + b*q1.b + c*q1.c + d*q1.d;
}

Quat Quat::cross(const Quat &q1) {
  Quat q2(0,
          c*q1.d - d*q1.c,
	  d*q1.b - b*q1.d,
	  b*q1.c - c*q1.b);
  return q2;
}

Quat Quat::rej(Quat q1) {
  Quat q2 = q1.unit();
  return (*this) - (this->dot(q2))*q2;
}

double Quat::abs(void) {
  return sqrt((*this).dot(*this));
}

Quat Quat::unit(void) {
  return (*this)*(1/(*this).abs());
}

Quat Quat::rotator(const double &cos_theta, const Quat &axis) {
  // compute cosine of half the angle (theta must be <= pi)
  double cos_theta_2 = sqrt((1+cos_theta)/2);
  // compute sine of half the angle (theta must be <= pi)
  double sin_theta_2 = sqrt((1-cos_theta)/2);
  Quat q(cos_theta_2, sin_theta_2*axis.b, sin_theta_2*axis.c,
         sin_theta_2*axis.d);
  return q;
}

Quat Quat::mean(const Quat q[], const int &len) {
  Quat sum;
  for (int i = 0; i < len; i++) {
    sum = sum + q[i];
  }
  return (1.0/len)*sum;
}

Quat operator+(const Quat &q1, const Quat &q2) {
  Quat q3(q1.a + q2.a, q1.b + q2.b,
          q1.c + q2.c, q1.d + q2.d);
  return q3;
}

Quat operator-(const Quat &q1, const Quat &q2) {
  Quat q3(q1.a - q2.a, q1.b - q2.b,
          q1.c - q2.c, q1.d - q2.d);
  return q3;
}

Quat operator*(const Quat &q1, const Quat &q2) {
  Quat q3(q1.a*q2.a - q1.b*q2.b - q1.c*q2.c - q1.d*q2.d,
          q1.a*q2.b + q1.b*q2.a + q1.c*q2.d - q1.d*q2.c,
          q1.a*q2.c - q1.b*q2.d + q1.c*q2.a + q1.d*q2.b,
          q1.a*q2.d + q1.b*q2.c - q1.c*q2.b + q1.d*q2.a);
  return q3;
}

Quat operator*(const double &x, const Quat &q1) {
  Quat q2(q1.a*x, q1.b*x, q1.c*x, q1.d*x);
  return q2;
}

Quat operator*(const Quat &q1, const double &x) {
  return x*q1;
}

Quat operator!(const Quat &q1) {
  Quat q2(q1.a, -q1.b, -q1.c, -q1.d);
  return q2;
}

std::ostream& operator<<(std::ostream &strm, const Quat &quat) {
  return strm << quat.a << " + " << quat.b << "i + " <<
         quat.c << "j + " << quat.d << "k";
}


