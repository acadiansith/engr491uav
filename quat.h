/**** quat.h *********************************************
 * Author: Daniel LeJeune
 * Creation: 2/24/14
 **** Changelog ******************************************
 * 
 * 2/24/14
 * ----------
 * +File creation
 *********************************************************/

#ifndef DL_QUATERNIONS
#define DL_QUATERNIONS

#include <iostream>
#include <cmath>

class Quat {
  public: 
    double a, b, c, d;
    Quat(void);
    Quat(double, double, double, double);
    double dot(const Quat &q1);
    Quat cross(const Quat &q1);
    Quat rej(Quat q1);
    double abs(void);
    Quat unit(void);
    static Quat rotator(const double &cos_theta, const Quat &axis);
    static Quat mean(const Quat q[], const int &len);
};

Quat operator+(const Quat &q1, const Quat &q2);
Quat operator-(const Quat &q1, const Quat &q2);
Quat operator*(const Quat &q1, const Quat &q2);
Quat operator*(const double &x, const Quat &q1);
Quat operator*(const Quat &q1, const double &x);
Quat operator!(const Quat &q1);
std::ostream& operator<<(std::ostream &strm, const Quat &quat);

#endif
