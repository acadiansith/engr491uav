/**** serial.cpp *****************************************
 * Author: Daniel LeJeune
 * Creation: 2/13/14
 **** Changelog ******************************************
 * 
 * 2/14/14
 * ----------
 * +Moved header information to serial.h
 * 
 * 2/13/14
 * ----------
 * +File creation
 * +Added wiringPi serial library
 * +Added test code
 *********************************************************/

/**** Includes *******************************************/
#include "serial.h"

#define SERIAL_LOOP_TIME 50000
#define SERIAL_LOOP_TIMEOUT 50

pthread_t serial_thread;

int RXTX;

bool serial_runnning;
bool serial_ready;
bool command_start;

struct DATA_RECEIVED {
  short int y;
  short int x;
  unsigned char yaw;
  unsigned char switches;
};

/**** Functions ******************************************/
void begin_serial(void) {
  serial_runnning = true;
  serial_ready = false;
  command_start = false;

  pthread_mutex_init(&controller_data_mutex, NULL);
  pthread_create(&serial_thread, NULL, serial_routine, NULL);
  while (!thread_controller_data.up || !thread_controller_data.down) {
    usleep(1000);
  }
  command_start = true;
}

void close_serial(void) {
  serial_runnning = false;
  pthread_join(serial_thread, NULL);
}

/****** Serial functions *********************************/
void* serial_routine(void* ptr) { 
  // Initialize serial communications
  RXTX = serialOpen(SERIAL_PORT, 9600);

  thread_controller_data.pitch = 0;
  thread_controller_data.roll = 0;
  thread_controller_data.sel_button = false;
  thread_controller_data.l_yaw = false;
  thread_controller_data.r_yaw = false;
  thread_controller_data.up = false;
  thread_controller_data.down = false;
  thread_controller_data.release = false;

  data_link = true;
  
  serial_ready = true;

  bool bad_run = false;
  
  while (serial_runnning) {
    
    serialPutchar(RXTX, 'b');
    usleep(SERIAL_LOOP_TIME);

    int i;
    for (i = 0; i < SERIAL_LOOP_TIMEOUT; i++) {
      int bytes = serialDataAvail(RXTX);

      if (bytes > 0) {
        if (serialGetchar(RXTX) == 'S') {
	  if ((bytes = serialDataAvail(RXTX) < 6)) {
	    usleep(SERIAL_LOOP_TIME);
	  }
	  if ((bytes = serialDataAvail(RXTX) < 6)) {
	    serialFlush(RXTX);
	    continue;
	  }
	  else {
            struct DATA_RECEIVED data;
	    read(RXTX, &data, sizeof(data));
            thread_controller_data.pitch = 
	      JOYSTICK_SCALE*(data.x - X_CENTER);
            thread_controller_data.roll = 
	      JOYSTICK_SCALE*(data.y - Y_CENTER);
            thread_controller_data.l_yaw =
	      (data.yaw & 0x01) > 0;
	    thread_controller_data.r_yaw =
	      (data.yaw & 0x02) > 0;
            thread_controller_data.up =
	      (data.switches & 0x01) > 0;
            thread_controller_data.down =
	      (data.switches & 0x02) > 0;
            thread_controller_data.sel_button =
	      (data.switches & 0x04) > 0;
            thread_controller_data.release =
	      (data.switches & 0x08) > 0;
	    serialFlush(RXTX);
	    bad_run = false;
	    break;
	  }
	}
      }
      
      usleep(SERIAL_LOOP_TIME);
    }
    if (i == SERIAL_LOOP_TIMEOUT && command_start) {
      if (bad_run) {
        cout << "He's dead, Jim!" << endl;
	data_link = false;
        break;
      }
      bad_run = true;
    }

  }
  
  serialClose(RXTX);
  
}

void serial_write_word(short int x) {
  union WORD_BUILDER wbld;
  wbld.word = x;
  serialPutchar(RXTX, wbld.bytes.low);
  serialPutchar(RXTX, wbld.bytes.high);
}

void serial_write_float(float x) {
  union FLOAT_BUILDER fbld;
  fbld.f = x;
  for (int i=0; i < sizeof(float); i++) {
    serialPutchar(RXTX, fbld.bytes[i]);
  }
}

void serial_write_byte(char x) {
  serialPutchar(RXTX, x);
}

