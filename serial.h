/**** serial.h *******************************************
 * Author: Daniel LeJeune
 * Creation: 2/14/14
 **** Changelog ******************************************
 * 
 * 2/14/14
 * ----------
 * +File creation
 *********************************************************/

#ifndef ENGR491_SERIAL
#define ENGR491_SERIAL

/**** Includes *******************************************/
#include <iostream>
#include <wiringSerial.h>
#include <pthread.h>
#include <cstdio>
#include "i2c.h"

/**** Macro definitions **********************************/

/**** Constant definitions *******************************/

#define SERIAL_PORT "/dev/ttyAMA0"
#define X_CENTER 520
#define Y_CENTER 514
#define JOYSTICK_SCALE 0.00039

using namespace std;

/**** Type definitions ***********************************/
union FLOAT_BUILDER {
  float f;
  char bytes[sizeof(float)];
};

typedef struct CONTROLLER_DATA {
  double pitch;
  double roll;
  bool sel_button;
  bool l_yaw;
  bool r_yaw;
  bool up;
  bool down;
  bool release;
} controller_data_t;

/**** Function prototypes ********************************/
void begin_serial(void);
void close_serial(void);

void* serial_routine(void* ptr);
void serial_write_word(short int x);
void serial_write_float(float x);
void serial_write_byte(char x);

/**** Global variables ***********************************/

extern controller_data_t thread_controller_data;
extern pthread_mutex_t controller_data_mutex;
extern bool data_link;

#endif
