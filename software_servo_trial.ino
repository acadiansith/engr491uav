//PROGRAM TO TEST ELECTRONIC SPEED CONTROL DEVICES
//BY STEPHEN TYLER

#include <Servo.h>

Servo escNorth; //Label ESC being tested

int degree = 40; //Set maximum value to be sent to ESC during this run of the program
void setup(){
  escNorth.attach(9); //Set the ESC to be attached to pin 9
  Serial.begin(9600); //Open Serial communication to monitor program
}

void loop(){
 int j;
 Serial.println("Begin"); //Not beginning of Program
 //List of different values tested:
 //40
 //50
 //60
 //70
 //80
 //90
 //100
 //120
 //130
 for(j = 0; j <=degree; j+= 5){ //Increase Value from 0 to maximum value for run through
   escNorth.write(j); //Set ESC to given value
   delay(1000); //Delay by 1 second
   Serial.println(j); //Print value that just ran
 }
 delay(15000); //Hold maximum value for 15 seconds
 for(j = degree; j >= 0; j -=5){ //Have value go down to 0 from maximum value
   escNorth.write(j); //Set ESC to given value
   delay(1000); //Delay 1 second
 }
 
 Serial.println("Done"); //Let user know when the program has stopped
 delay(10000);//Delay 10 seconds before starting again
}
