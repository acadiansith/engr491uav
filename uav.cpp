/**** uav.cpp ********************************************
 * Author: Daniel LeJeune
 * Creation: 2/14/14
 **** Changelog ******************************************
 * 
 * 2/14/14
 * ----------
 * +File creation
 *********************************************************/

#include <iostream>
#include <pthread.h>
#include <wiringPi.h>
#include "i2c.h"
#include "serial.h"
#include "quat.h"
#include <time.h>
#include <stdlib.h>

#define OMEGA_FROM_GYRO 0.001214142
#define MASS 1.4
#define PI 3.14159265359
#define YAW_RATE 0.5
#define ALT_RATE 0.5
#define LOW_FORCE 0
#define HIGH_FORCE 4.2

// accelerometer data queue
three_axis_data accel_queue[ACCEL_QUEUE_LEN];
int accel_queue_head;
pthread_mutex_t accel_queue_mutex;

// gyroscope data queue
three_axis_data gyro_queue[GYRO_QUEUE_LEN];
int gyro_queue_head;
pthread_mutex_t gyro_queue_mutex;

// compass data queue
three_axis_data compass_queue[COMPASS_QUEUE_LEN];
int compass_queue_head;
pthread_mutex_t compass_queue_mutex;

int motor_values[4];
bool release;
bool motor_start;
pthread_mutex_t arduino_mutex;

controller_data_t thread_controller_data;
pthread_mutex_t controller_data_mutex;
bool data_link;

typedef struct PID_CONTROLLER {
  double P;
  double I;
  double D;
  double N;
  double y0p;
  double y0i;
  double y0d;
  double x0;
} pid_controller_t;

Quat get_accel_vector(three_axis_data &v);
Quat get_gyro_vector(three_axis_data &v);
Quat get_compass_vector(three_axis_data &v);
double get_elapsed_time(void);
double update_PID(pid_controller_t &c, double x, double del_t);
double sin_from_cos(double x);
double saturate(double x, double low, double high);
Quat forces_from_accel(Quat &q);
double alt_accel_from_forces(Quat &q, double cosine);
void set_motors_from_forces(Quat forces);

const Quat compass_center(0, -135, -91, 38);
//const Quat mag_north = Quat(0, 646.8, 24210.3, -41208.8).unit();

int main(int argc, char* argv[]) {

  motor_start = false;

  begin_i2c();
  begin_serial();

  motor_start = true;

  pid_controller_t yaw_controller = {
    25,
    25,
    15,
    50,
    0,
    0,
    0, 
    0
  };
  pid_controller_t roll_controller = {
    40,
    50,
    15,
    50,
    0,
    0,
    0, 
    0
  };
  pid_controller_t pitch_controller = roll_controller;
  pid_controller_t alt_controller = {
    12,
    50,
    0,
    50,
    0,
    0,
    0,
    0
  };

  double yaw_sp = 0;
  double roll_sp = 0;
  double pitch_sp = 0;
  double alt_v_sp = 0;

  Quat a_array[ACCEL_QUEUE_LEN];
  Quat g_array[GYRO_QUEUE_LEN];
  Quat c_array[COMPASS_QUEUE_LEN];

  pthread_mutex_lock(&accel_queue_mutex);
  for (int i = 0; i < ACCEL_QUEUE_LEN; i++) {
    a_array[i] = get_accel_vector(
      accel_queue[i]);
  }
  pthread_mutex_unlock(&accel_queue_mutex);

  pthread_mutex_lock(&gyro_queue_mutex);
  for (int i = 0; i < GYRO_QUEUE_LEN; i++) {
    g_array[i] = get_gyro_vector(
      gyro_queue[i]);
  }
  pthread_mutex_unlock(&gyro_queue_mutex);

  pthread_mutex_lock(&compass_queue_mutex);
  for (int i = 0; i < COMPASS_QUEUE_LEN; i++) {
    c_array[i] = get_compass_vector(
      compass_queue[i]);
  }
  pthread_mutex_unlock(&compass_queue_mutex);

  Quat a = Quat::mean(a_array, ACCEL_QUEUE_LEN);
  Quat c = Quat::mean(c_array, COMPASS_QUEUE_LEN);
  Quat g = Quat::mean(g_array, GYRO_QUEUE_LEN);

  //*
  Quat down(0, 0, 0, -1);
 
  //Quat q = Quat::rotator(a.unit().dot(down),
  //  a.cross(down).unit());
  //Quat true_down = q*a*!q;

  Quat q = Quat(1, 0, 0, 0);
  Quat true_down = a;
  
  Quat due_down = true_down.unit();
  Quat mag_north = (q*c*!q).unit();
  Quat due_east = down.cross(mag_north).unit();
  Quat due_north = due_east.cross(due_down).unit();

  Quat yaw_rej = (q*due_north*!q).rej(due_down);
  double yaw = PI;
  yaw_sp = yaw;
  int revs = 0;
  double z_accel = -5;
  //*/

  /*
  Quat q1 = Quat::rotator(c.unit().dot(mag_north),
    c.cross(mag_north).unit());
  Quat a1 = q1*a*!q1;

  Quat down(0, 0, 0, -1);
  Quat a1_rej = (a1 - a1.dot(mag_north)*mag_north).unit();
  Quat down_rej = (down - down.dot(mag_north)*mag_north).unit();

  Quat q2 = Quat::rotator(down_rej.dot(a1_rej),
    a1_rej.cross(down_rej));

  Quat q = q2*q1;

  //*/
  get_elapsed_time();

  Quat gyro_bias = g;

  double z_vel = 0;

  for (int i = 0; i < 10000000; i++) {
    
    if (!data_link) break;
    
    pthread_mutex_lock(&accel_queue_mutex);
    a = get_accel_vector(
      accel_queue[accel_queue_head]);
    pthread_mutex_unlock(&accel_queue_mutex);

    pthread_mutex_lock(&gyro_queue_mutex);
    g = get_gyro_vector(
      gyro_queue[gyro_queue_head]);
    pthread_mutex_unlock(&gyro_queue_mutex);

    pthread_mutex_lock(&compass_queue_mutex);
    c = get_compass_vector(
      compass_queue[compass_queue_head]);
    pthread_mutex_unlock(&compass_queue_mutex);

    a_array[accel_queue_head] = a;
    g_array[gyro_queue_head] = g;
    c_array[compass_queue_head] = c;

    g = g - gyro_bias;

    double del_t = get_elapsed_time();

    // subtract because the acceleration is the negative of the difference
    // between the measured value and g

    double del_theta = del_t * g.abs();
    Quat g_mod = del_theta / 2 * g.unit();
    //Quat c_mod = 10 * del_theta / 2 * 
    //  (q*c*!q).unit().cross(mag_north);
    Quat mag_rej = (mag_north - mag_north.dot(due_down)*due_down).unit();
    Quat c2 = q*c*!q;
    Quat c_rej = (c2 - c2.dot(due_down)*due_down).unit();
    Quat c_mod = 10 * del_theta / 2 *
      c_rej.cross(mag_rej);
    Quat a_mod = 3 * del_theta / 2 *
      (q*a*!q).unit().cross(due_down);

    q = q*Quat(1, g_mod.b, g_mod.c, g_mod.d) + 

      (c_mod + a_mod)*q;
    q = q.unit();

    Quat norm_north(0, 0, 1, 0);
    Quat norm_east(0, 1, 0, 0);
    Quat norm_down(0, 0, 0, -1);
    
    Quat new_north = q*norm_north*!q;
    Quat new_east = q*norm_east*!q;
    Quat flat_north = new_north.rej(norm_down).unit();
    Quat flat_east = new_east.rej(norm_down).unit();

    double new_yaw = fmod(atan2(flat_north.b, -flat_north.c)+2*PI, 2*PI);
    double del_yaw = new_yaw - fmod(yaw-(2*PI*(revs-1)), 2*PI);
    if (del_yaw > PI) revs--;
    else if (del_yaw < -PI) revs++;
    yaw = 2*PI*revs + new_yaw;

    double pitch = -new_north.dot(norm_down);

    double roll = -new_east.dot(norm_down);

    yaw_sp += YAW_RATE*del_t*(
      (thread_controller_data.l_yaw ? 1 : 0) +
      (thread_controller_data.r_yaw ? -1 : 0)
      );
    z_accel += ALT_RATE*del_t*(
      (thread_controller_data.up ? 1 : 0) +
      (thread_controller_data.down ? -1 : 0)
      );
    roll_sp = thread_controller_data.roll;
    pitch_sp = thread_controller_data.pitch;

    double accel_yaw = 
      update_PID(yaw_controller, yaw_sp - yaw, del_t);
    double accel_pitch = 
      update_PID(pitch_controller, pitch_sp - pitch, del_t);
    double accel_roll = 
      update_PID(roll_controller, roll_sp - roll, del_t);
    double accel_alt = 
    //  update_PID(alt_controller, alt_v_sp - z_vel, del_t);
      z_accel;
    double down_dot = norm_down.dot(q*norm_down*!q);
    Quat accels(
      accel_yaw*0.63,
      accel_roll*0.035,
      accel_pitch*0.035,
      (accel_alt+9.8)*MASS/down_dot
    );

    Quat forces = forces_from_accel(accels);

    set_motors_from_forces(forces);
    release = thread_controller_data.release;
 
    //cout << q << endl;

    if (i % 6 == 0) {

    cout << forces << endl;

    //cout << "Pitch: " << pitch << endl;
    //cout << "Roll: " << roll << endl;
    //cout << "Yaw: " << yaw << endl;

    /*
      serial_write_float(q.a);
      serial_write_float(q.b);
      serial_write_float(q.c);
      serial_write_float(q.d);
      serial_write_float(acos((q*a*!q).unit().dot(due_down)));
      serial_write_float(acos((q*c*!q).unit().dot(mag_north)));
      serial_write_float(alt);
      */

    }

    //cout << q.a << endl;

    struct timespec sleeper;
    sleeper.tv_sec = 0;
    sleeper.tv_nsec = 10000000;
    nanosleep(&sleeper, NULL);
  }

  close_i2c();
  close_serial();

  system("poweroff");

  return 0;
}

Quat get_accel_vector(three_axis_data &v) {
  Quat a(0, -v.x.word,
            -v.y.word,
            -v.z.word);
  return 0.0078125*a;  
}

Quat get_gyro_vector(three_axis_data &v) {
  Quat g(0, v.x.word,
            v.y.word,
            v.z.word);
  return OMEGA_FROM_GYRO*g;
}

Quat get_compass_vector(three_axis_data &v) {
  Quat c(0, v.y.word,
            -v.x.word,
            v.z.word);
  return c - compass_center;
}

double get_elapsed_time(void) {
  static struct timespec last_time;
  struct timespec next_time;
  
  clock_gettime(CLOCK_MONOTONIC, &next_time);

  // borrow a second if needed
  if (next_time.tv_nsec < last_time.tv_nsec) {
    next_time.tv_nsec += 1000000000L;
    next_time.tv_sec -= 1;
  }
  
  double elapsed = (next_time.tv_sec - last_time.tv_sec) +
    1e-9*(next_time.tv_nsec - last_time.tv_nsec);

  last_time = next_time;

  return elapsed;
}

double update_PID(pid_controller_t &c, double x, double del_t) {
  
  double yp = c.P*x;
  double yi = .999*c.y0i + c.I*del_t*c.x0;
  yi = saturate(yi, -c.I/5, c.I/5);
  double yd = (1-c.N*del_t)*c.y0d + c.D*c.N*(x-c.x0);
  double y = yp + yi + yd;
  c.x0 = x;
  c.y0p = yp;
  c.y0i = yi;
  c.y0d = yd;

  return y;
}

double sin_from_cos(double x) {
  return sqrt(1 - x*x);
}

double saturate(double x, double low, double high) {
  return (x > high) ? high : ((x < low) ? low : x);
}

Quat forces_from_accel(Quat &q) {
  Quat q1(q.a*0.25 + q.c*0.5 + q.d*0.25,
              -q.a*0.25 + q.b*0.5 + q.d*0.25,
              q.a*0.25 - q.c*0.5 + q.d*0.25,
              -q.a*0.25 - q.b*0.5 + q.d*0.25);
  return Quat(saturate(q1.a, LOW_FORCE, HIGH_FORCE),
              saturate(q1.b, LOW_FORCE, HIGH_FORCE),
              saturate(q1.c, LOW_FORCE, HIGH_FORCE),
              saturate(q1.d, LOW_FORCE, HIGH_FORCE));
}

double alt_accel_from_forces(Quat &q, double cosine) {
  return (q.a+q.b+q.c+q.d)*cosine/MASS - 9.8;
}

void set_motors_from_forces(Quat forces) {
  motor_values[0] = 40 + (60.0/HIGH_FORCE)*forces.a - 0.1;
  motor_values[1] = 40 + (60.0/HIGH_FORCE)*forces.b - 0.1;
  motor_values[2] = 40 + (60.0/HIGH_FORCE)*forces.c - 0.1;
  motor_values[3] = 40 + (60.0/HIGH_FORCE)*forces.d - 0.1;
}
